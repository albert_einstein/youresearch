module youresearch

go 1.13

require (
	cloud.google.com/go v0.61.0 // indirect
	cloud.google.com/go/datastore v1.1.0
	github.com/blevesearch/bleve v1.0.9
	github.com/cznic/b v0.0.0-20181122101859-a26611c4d92d // indirect
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548 // indirect
	github.com/cznic/strutil v0.0.0-20181122101858-275e90344537 // indirect
	github.com/facebookgo/ensure v0.0.0-20200202191622-63f1cf65ac4c // indirect
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/subset v0.0.0-20200203212716-c811ad88dec4 // indirect
	github.com/google/go-cmp v0.5.1 // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/jmhodges/levigo v1.0.0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	github.com/tecbot/gorocksdb v0.0.0-20191217155057-f0fad39f321c // indirect
	golang.org/x/tools v0.0.0-20200727233628-55644ead90ce // indirect
	google.golang.org/api v0.29.0
	google.golang.org/genproto v0.0.0-20200728010541-3dc8dca74b7b // indirect
)
