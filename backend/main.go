package main

import (
	"context"
	"crypto/sha1"
	"encoding/csv"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"cloud.google.com/go/datastore"

	"github.com/blevesearch/bleve"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

//TODO: CACHE
//TODO: COUNT

var c *datastore.Client

func health(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "all ok")
}

type Profile struct {
	BirthYear     int
	Height        int
	Weight        int
	DiagnosisYear int
	IllYear       int
	Diagnosis     string
	Symptoms      string `datastore:",noindex"`
	Medication    string `datastore:",noindex"`
	OtherDiseases string `datastore:",noindex"`
}
type Location struct {
	Lat float64 `datastore:",noindex"`
	Lon float64 `datastore:",noindex"`
	Acc float64 `datastore:",noindex"`
	Alt float64 `datastore:",noindex"`
}

type Answers map[string]float64

type Answer struct {
	DeviceID string
	FCMToken string `datastore:",noindex" json:",omitempty"`
	Time     int64
	Answers  Answers  `datastore:",flatten"`
	Profile  Profile  `datastore:",flatten"`
	Location Location `datastore:",flatten"`
}

func (a *Answers) Load(pp []datastore.Property) error {
	if *a == nil {
		*a = map[string]float64{}
	}
	for _, p := range pp {
		v, ok := p.Value.(float64)
		if !ok {
			return fmt.Errorf("answer should be float64")
		}
		(*a)[p.Name] = v
	}
	return nil // TODO: load
}

func (a *Answers) Save() ([]datastore.Property, error) {
	props := datastore.PropertyList{}
	for k, v := range *a {
		props = append(props, datastore.Property{Name: k, Value: v, NoIndex: true})
	}
	return props, nil
}

func jsonErr(w http.ResponseWriter, err error, code int) {
	w.WriteHeader(code)
	_ = json.NewEncoder(w).Encode(map[string]string{
		"error": err.Error(),
	})
}

func saveAnswer(w http.ResponseWriter, r *http.Request) {
	var a Answer
	err := json.NewDecoder(r.Body).Decode(&a)
	if err != nil {
		jsonErr(w, err, 400)
		return
	}
	a.Time = time.Now().Unix()
	h := sha1.New()
	h.Write([]byte(a.DeviceID))
	a.DeviceID = hex.EncodeToString(h.Sum(nil))
	key, err := c.Put(r.Context(), datastore.IncompleteKey("Answers", nil), &a)
	if err != nil {
		jsonErr(w, err, 500)
		return
	}
	yourAnswers, err := c.Count(r.Context(), datastore.NewQuery("Answers").Filter("DeviceID =", a.DeviceID))
	if err != nil {
		jsonErr(w, err, 500)
		return
	}
	monthAnswers, err := c.Count(r.Context(), datastore.NewQuery("Answers").Filter("Time >", time.Now().Add(-time.Hour*24*31).Unix()))
	if err != nil {
		jsonErr(w, err, 500)
		return
	}
	_ = json.NewEncoder(w).Encode(map[string]int{
		"YourAnswers":   yourAnswers,
		"MonthsAnswers": monthAnswers,
	})
	log.Printf("%#v %#v", a, key)
}

var indexes = map[string]bleve.Index{}

func suggestInput(w http.ResponseWriter, r *http.Request) {
	req := struct {
		Text string
		Type string
	}{}
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		jsonErr(w, err, 400)
		return
	}

	index, ok := indexes[req.Type]
	if !ok {
		jsonErr(w, fmt.Errorf("index type %v not found", req.Type), 400)
		return
	}

	q := bleve.NewWildcardQuery("*" + strings.TrimSpace(req.Text) + "*")
	sReq := bleve.NewSearchRequest(q)
	sReq.Size = 5
	sRes, err := index.Search(sReq)
	if err != nil {
		jsonErr(w, err, 400)
		return
	}
	ret := []string{}
	for _, v := range sRes.Hits {
		ret = append(ret, v.ID)
	}
	_ = json.NewEncoder(w).Encode(ret)
}

type Medication struct {
	Regnumber                interface{} `json:"regnumber"`
	Regdate                  interface{} `json:"regdate"`
	Enddate                  interface{} `json:"enddate"`
	Cancellationdate         interface{} `json:"cancellationdate"`
	Nameregcertificate       string      `json:"nameregcertificate"`
	Country                  string      `json:"country"`
	Tradename                interface{} `json:"tradename"`
	Internationalname        interface{} `json:"internationalname"`
	Formrelease              interface{} `json:"formrelease"`
	Stages                   string      `json:"stages"`
	Barcodes                 interface{} `json:"barcodes"`
	Normativedocumentation   string      `json:"normativedocumentation"`
	Pharmacotherapeuticgroup string      `json:"pharmacotherapeuticgroup"`
}

func listAnswers(ctx context.Context, q *datastore.Query, f func(a Answer) error) error {
	var cursor datastore.Cursor
	var a Answer
	q = q.Limit(500)
	for i := 0; ; i++ {
		if i != 0 {
			log.Print("s")
			q = q.Start(cursor)
		}
		it := c.Run(ctx, q)
		_, err := it.Next(&a)
		if err == iterator.Done { // got 0 results
			return nil
		}
		for err == nil {
			a.FCMToken = ""
			encErr := f(a)
			if encErr != nil {
				return err
			}
			a = Answer{} // cleanup
			_, err = it.Next(&a)
		}
		if err != iterator.Done {
			return err
		}
		cursor, err = it.Cursor()
		if err != nil {
			return err
		}
	}
}

func reportAllJSON(w http.ResponseWriter, r *http.Request) {
	enc := json.NewEncoder(w)
	w.Header().Set("Content-Type", "text/json; charset=utf-8")
	w.Header().Set("Content-Disposition", fmt.Sprintf(`attachment; filename="youresearch-answers-%v.json`, time.Now().Format("2006-01-02")))
	err := listAnswers(r.Context(), datastore.NewQuery("Answers").Order("Time"), func(a Answer) error {
		return enc.Encode(a)
	})
	if err != nil {
		jsonErr(w, err, 500)
	}
}

func reportAllCSV(w http.ResponseWriter, r *http.Request) {
	keys := map[string]bool{}
	aa := []Answer{}
	w.Header().Set("Content-Type", "text/csv; charset=utf-8")
	w.Header().Set("Content-Disposition", fmt.Sprintf(`attachment; filename="youresearch-answers-%v.csv`, time.Now().Format("2006-01-02")))
	err := listAnswers(r.Context(), datastore.NewQuery("Answers"), func(a Answer) error {
		aa = append(aa, a)
		for k := range a.Answers {
			_, ok := keys[k]
			if !ok {
				keys[k] = true
			}
		}
		return nil
	})
	if err != nil {
		jsonErr(w, err, 500)
		return
	}

	kk := []string{}
	for k := range keys {
		kk = append(kk, k)
	}

	cw := csv.NewWriter(w)
	err = cw.Write(append([]string{
		"Time",
		"ID",
		"Profile.BirthYear",
		"Profile.Height",
		"Profile.Weight",
		"Profile.DiagnosisYear",
		"Profile.IllYear",
		"Profile.Diagnosis",
		"Profile.Symptoms",
		"Profile.Medication",
		"Profile.OtherDiseases",
		"Location.Lat",
		"Location.Lon"}, kk...))
	if err != nil {
		jsonErr(w, err, 500)
		return
	}
	for _, a := range aa {
		rec := []string{}
		rec = append(rec,
			time.Unix(a.Time, 0).Format("2006-01-02"),
			a.DeviceID,
			fmt.Sprint(a.Profile.BirthYear),
			fmt.Sprint(a.Profile.Height),
			fmt.Sprint(a.Profile.Weight),
			fmt.Sprint(a.Profile.DiagnosisYear),
			fmt.Sprint(a.Profile.IllYear),
			a.Profile.Diagnosis,
			a.Profile.Symptoms,
			a.Profile.Medication,
			a.Profile.OtherDiseases,
			fmt.Sprint(a.Location.Lat), fmt.Sprint(a.Location.Lon))
		for _, k := range kk {
			rec = append(rec, fmt.Sprint(a.Answers[k]))
		}
		err := cw.Write(rec)
		if err != nil {
			jsonErr(w, err, 500)
			return
		}
	}
	cw.Flush()
}

func reportMy(w http.ResponseWriter, r *http.Request) {
	keys := map[string]bool{}
	aa := []Answer{}
	h := sha1.New()
	h.Write([]byte(mux.Vars(r)["deviceID"]))
	id := hex.EncodeToString(h.Sum(nil))

	w.Header().Set("Content-Type", "text/csv; charset=utf-8")
	w.Header().Set("Content-Disposition", fmt.Sprintf(`attachment; filename="youresearch-answers-%v.csv`, time.Now().Format("2006-01-02")))
	err := listAnswers(r.Context(), datastore.NewQuery("Answers").Filter("DeviceID =", id), func(a Answer) error {
		aa = append(aa, a)
		for k := range a.Answers {
			_, ok := keys[k]
			if !ok {
				keys[k] = true
			}
		}
		return nil
	})
	if err != nil {
		jsonErr(w, err, 500)
		return
	}

	kk := []string{}
	for k := range keys {
		kk = append(kk, k)
	}

	cw := csv.NewWriter(w)
	err = cw.Write(append([]string{"Time", "Location.Lat", "Location.Lon"}, kk...))
	if err != nil {
		jsonErr(w, err, 500)
		return
	}
	for _, a := range aa {
		rec := []string{}
		rec = append(rec, time.Unix(a.Time, 0).Format("2006-01-02"), fmt.Sprint(a.Location.Lat), fmt.Sprint(a.Location.Lon))
		for _, k := range kk {
			rec = append(rec, fmt.Sprint(a.Answers[k]))
		}
		err := cw.Write(rec)
		if err != nil {
			jsonErr(w, err, 500)
			return
		}
	}
	cw.Flush()
}

func main() {
	ctx := context.Background()
	client, err := datastore.NewClient(ctx, "noted-processor-92815", option.WithCredentialsFile("client_secret.json"))
	if err != nil {
		panic(err)
	}
	c = client

	symIndex, err := bleve.NewMemOnly(bleve.NewIndexMapping())
	if err != nil {
		panic(err)
	}
	symptoms := []string{"Агрессивность", "Нарушения месячного цикла", "Проблемы с памятью", "Зуд", "Тревожность", "Вздутие живота", "Воспаление гланд", "Галюцинации", "Гиперактивность", "Головокружения", "Дизориентация и спутанность сознания", "Депрессия", "Кожные высыпания", "Жжение в теле", "Боль в груди", "Запор", "Затруднение носового дыхания", "Затруднение при общении", "Изменение походки", "Изменение речи", "Изменение цвета мочи", "Кашель", "Метеоризм", "Мокрота", "Опухание тела", "Навязчивые мысли", "Налет на языке", "Потоотделение", "Обмороки", "Покалывания/Мурашки", "Утомление глаз", "Проблемы со сном", "Растягивание звука речи", "Речевые нарушения", "Светобоязнь", "Выпадение волос", "Эректильная дисфункция", "Проблемы со зрением", "Тошнота", "Трещины в уголках рта", "Увеличение грудних желез", "Чихание", "Эйфория", "Эмоциональная нестабильность", "Сонливость", "Онемение", "Паралич", "Мышечные спазмы", "Нарушение координации", "Слабость", "Быстрая утомляемость", "Проблемы с кишечником", "Снижение чувствительности", "Головные боли", "Нервные боли", "Тремор", "Температура"}
	for _, s := range symptoms {
		_ = symIndex.Index(s, s)
	}
	indexes["symptoms"] = symIndex

	diagIndex, err := bleve.NewMemOnly(bleve.NewIndexMapping())
	if err != nil {
		panic(err)
	}
	diangosis := []string{"Ремиттирующий-рецидивирующий РС", "Вторично-прогрессирующий РС", "Первично-прогрессирующий"}
	for _, d := range diangosis {
		_ = diagIndex.Index(d, d)
	}
	indexes["diagnosis"] = diagIndex

	medIndex, err := bleve.NewMemOnly(bleve.NewIndexMapping())
	if err != nil {
		panic(err)
	}
	medications := []string{"ПИТРС", "Протокол Коимбра", "Народная медицина", "Нет"}
	for _, s := range medications {
		_ = medIndex.Index(s, s)
	}
	indexes["medication"] = medIndex

	diseasesIndex, err := bleve.NewMemOnly(bleve.NewIndexMapping())
	if err != nil {
		panic(err)
	}
	diseases := []string{"Сахарный диабет", "Аллергия", "Нет"}
	for _, s := range diseases {
		_ = diseasesIndex.Index(s, s)
	}
	indexes["diseases"] = diseasesIndex

	log.Print("started")
	r := mux.NewRouter()
	r.HandleFunc("/health", health)
	r.HandleFunc("/answer", saveAnswer)
	r.HandleFunc("/reports/all/json", reportAllJSON)
	r.HandleFunc("/reports/all/csv", reportAllCSV)
	r.HandleFunc("/reports/my/{deviceID}", reportMy)
	r.HandleFunc("/suggest", suggestInput)
	http.ListenAndServe(":8080", handlers.CORS()(r))
}
