// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:ffi';

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:device_id/device_id.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:string_similarity/string_similarity.dart';


Future<List<String>> suggest(String type, String text) async {
  var input = text.split("\n").last;
  var res = await http.post(
    'https://youresearch-64l7ehzjta-uc.a.run.app/suggest',
    body: jsonEncode({
      "Text": input,
      "Type": type,
    }),
  );
  var ret = List<dynamic>();
  ret = json.decode(res.body);
  return ret.map<String>((s) {return s.toString();}).toList();
}

Future<List<int>> getRootList() async {
  var res = await http.get('https://storage.googleapis.com/artem_and_co/current_config.json');
  return res.bodyBytes;
}

Future<String> fetchDog() async {
  var res = await http.get('https://dog.ceo/api/breeds/image/random');
  return jsonDecode(res.body)["message"];
}

Future<Map<String,dynamic>> getProfile() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String profile = prefs.getString("profile");
  if (profile == null) {
    var p = {
      "Gender": "",
      "BirthYear": 0,
      "Height": 0,
      "Weight": 0,
      "DiagnosisYear":0,
      "IllYear":0,
      "Diagnosis":"",
      "Symptoms":"",
      "Medication":"",
      "OtherDiseases":"",
      "Notify":true,
      "NotifyHour": 21,
      "NotifyMinute": 0,
    };
    setupLocalNotifications(Time(p["NotifyHour"], p["NotifyMinute"], 0));
    return p;
  }
  return jsonDecode(profile);
}

saveProfile(Map<String,dynamic> profile) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("profile", jsonEncode(profile));
}
  

Future<LocationData> getLocation() async {
    Location location = new Location();
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  _serviceEnabled = await location.serviceEnabled();
  if (!_serviceEnabled) {
    _serviceEnabled = await location.requestService();
    if (!_serviceEnabled) {
      return null;
    }
  }
  _permissionGranted = await location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    _permissionGranted = await location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      return null;
    }
  }

  return location.getLocation();
}

Future<http.Response> saveResults(Map<String,double> answers) async {
  var loc = await getLocation();
  print(answers);
  String deviceid = await DeviceId.getID;
  String fcmtoken = await FirebaseMessaging().getToken();

  var res = {
    "DeviceID": deviceid,
    "FCMToken": fcmtoken,
    "Answers": answers,
    "Location": {
          "Lat": loc.latitude,
          "Lon": loc.longitude,
          "Acc": loc.accuracy,
          "Alt": loc.altitude,
    },
    "Profile": await getProfile()};
  return http.post(
    'https://youresearch-64l7ehzjta-uc.a.run.app/answer',
    body: jsonEncode(res),
  );
}


class AnswerWidget extends StatefulWidget {
    final Map<String,dynamic> config;
    AnswerWidget(Map<String,dynamic> config) : config=config;

    @override
    AnswerWidgetState createState() => AnswerWidgetState();
}

class AnswerWidgetState extends State<AnswerWidget> {
  bool sendingAnswers;
  bool successSend;
  int monthAnswers;
  int yourAnswers;

    @override
    Widget build(BuildContext context) {
            if (sendingAnswers == true) {
              return Scaffold(
                  appBar: AppBar(
                title: Row(
                      children: <Widget>[
                        Expanded(child: Text("")),
                        Text("Отправка данных"),
                        Expanded(child: Text("")),
                      ]
                    ),
                  ),
                body:  Row(
                children: <Widget>[
                Expanded(child: Text("")),
                  Column(
                    children: <Widget>[
                      Expanded(child: Text("")),
                      SizedBox(
                        child: CircularProgressIndicator(strokeWidth: 15),
                        height: 100.0,
                        width: 100.0,
                      ),
                      Expanded(child: Text("")),
                    ],
                  ),
                  Expanded(child: Text("")),
                ],
              )
              );
            }
            
            if (successSend == true) {
                return Scaffold(
                    appBar: AppBar(
                  title: Row(
                        children: <Widget>[
                          Expanded(child: Text("")),
                          Text("💖"),
                          Expanded(child: Text("")),
                        ]
                      ),
                    ),
                    floatingActionButton: FloatingActionButton(
                    onPressed: () async {
                        setState((){
                          successSend = false;
                        });
                      },
                    tooltip: 'Отправить',
                    child: const Icon(Icons.done),
                    ),
                  body:  Column(
                      children: <Widget>[
                          Row(
                            children: <Widget>[ Text("")
                            ]
                          ),
                          Row(
                            children: <Widget>[
                            Expanded(child: Text("")),
                            Text("Большоe СПАСИБО!", textScaleFactor: 2, textAlign: TextAlign.center,),
                            Expanded(child: Text("")),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                            Expanded(child: Text("")),
                            Text("за твой $yourAnswersй вклад в исследование", textScaleFactor: 1.3, textAlign: TextAlign.center,),
                            Expanded(child: Text("")),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                            Expanded(child: Text("")),
                            Text("За месяц уже собрано $monthAnswers образцов", textScaleFactor: 1.3, textAlign: TextAlign.center,),
                            Expanded(child: Text("")),
                            ],
                          ),
                          Row(
                            children: <Widget>[ Text("")
                            ]
                          ),
                          Row(
                            children: <Widget>[
                            Expanded(child: Text("")),
                            Text("Продолжай в том же духе 🙂", textScaleFactor: 1.3, textAlign: TextAlign.center,),
                            Expanded(child: Text("")),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                            Expanded(child: Text("")),
                            Text("", textScaleFactor: 1.3, textAlign: TextAlign.center,),
                            Expanded(child: Text("")),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                            Expanded(child: Text("")),
                            FutureBuilder<String>(
                              future: fetchDog(),
                              builder: (context, snapshot) {
                                if (!snapshot.hasData) {
                                  return Text("");
                                }
                                return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      // show another dog on tap:)
                                    });
                                  },
                                  child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 500,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    fit: BoxFit.fitWidth,
                                    alignment: Alignment.topCenter,
                                    image: NetworkImage(snapshot.data),
                                  ),
                                ),
                              ));
                              }),
                            Expanded(child: Text("")),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                            Expanded(child: Text("")),
                            GestureDetector(
                              onTap: () {
                                launch("https://youresearch-64l7ehzjta-uc.a.run.app/reports/all/csv");
                              },
                              child:  Text("Скачать все результаты", style: TextStyle(decoration: TextDecoration.underline), textScaleFactor: 1.2, textAlign: TextAlign.center,),
                            ),
                            Expanded(child: Text("")),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                            Expanded(child: Text("")),
                            GestureDetector(
                              onTap: () async {
                                var id = await DeviceId.getID;
                                launch("https://youresearch-64l7ehzjta-uc.a.run.app/reports/my/$id");
                              },
                              child:  Text("Скачать все ваши результаты", style: TextStyle(decoration: TextDecoration.underline), textScaleFactor: 1.2, textAlign: TextAlign.center,),
                            ),
                            Expanded(child: Text("")),
                            ],
                          ),
                      ],
                    ),
                );
            }
              return Scaffold(
                  appBar: AppBar(
                    title: Row(
                      children: <Widget>[
                        Expanded(child: Text("")),
                        Text("Твой день"),
                        Expanded(
                          child: GestureDetector(
                          onTap: () {
                            mainCtl.nextPage(duration: Duration(milliseconds: 200), curve: Curves.ease);
                          },
                          child: Text(""),
                        )),
                        GestureDetector(
                          onTap: () {
                            mainCtl.nextPage(duration: Duration(milliseconds: 200), curve: Curves.ease);
                          },
                          child: Icon(Icons.arrow_forward_ios),
                        )
                      ]
                    ),
                  ),
                  body: ListWidget(widget.config),
                  floatingActionButton: FloatingActionButton(
                  onPressed: () async {
                      setState((){
                        sendingAnswers = true;
                        });
                      try {
                        var res = await saveResults(Answers.current);
                        if (res.statusCode < 200 || res.statusCode > 299) {
                          throw("Error saving answers with code " + res.statusCode.toString() + ": " + res.body);
                        }
                        var out = json.decode(res.body);
                        yourAnswers = out["YourAnswers"];
                        monthAnswers = out["MonthsAnswers"];
                      } catch(e) {
                        setState((){
                          sendingAnswers = false;  // TODO: show error message
                        });
                        throw(e);
                      }
                      Answers.current = Map<String,double>();
                      setState((){
                        sendingAnswers = false;
                        successSend = true;
                      });
                    },
                  tooltip: 'Отправить',
                  child: const Icon(Icons.send),
                  ),
                );
       
    }
}

class MSPeriod {
}

class ProfileWidget extends StatefulWidget {
    @override
    ProfileWidgetState createState() => ProfileWidgetState();
}


class ProfileWidgetState extends State<ProfileWidget> with AutomaticKeepAliveClientMixin {
  bool get wantKeepAlive => true;
  // String name;          // optional, public
  // String gender;
  // DateTime birthday;
  // Double height;
  // Double weight;


  // DateTime msStart;
  // String diagnosis; 
  // List<String> symptoms;
  // List<String> medication;
  // List<String> otherDiseases;
  
    @override
    Widget build(BuildContext context) {
      final _formKey = GlobalKey<FormState>();
      return FutureBuilder<Map<String,dynamic>>(
          future: getProfile(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return CircularProgressIndicator();
            }       
            var profile = snapshot.data;
            final TextEditingController _diagnosisCtl = TextEditingController();
            _diagnosisCtl.text = profile["Diagnosis"];
            final TextEditingController _symptomsCtl = TextEditingController();
            _symptomsCtl.text = profile["Symptoms"];
            final TextEditingController _medicationCtl = TextEditingController();
            _medicationCtl.text = profile["Medication"];
            final TextEditingController _diseasesCtl = TextEditingController();
            _diseasesCtl.text = profile["OtherDiseases"];
            
              return Scaffold(
                  appBar: AppBar(
                    title: Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            mainCtl.previousPage(duration: Duration(milliseconds: 200), curve: Curves.ease);
                          },
                          child: Icon(Icons.arrow_back_ios),
                        ),
                        Expanded(
                          child: GestureDetector(
                          onTap: () {
                            mainCtl.previousPage(duration: Duration(milliseconds: 200), curve: Curves.ease);
                          },
                          child: Text(""),
                        )),
                        Text("Твой Профиль"),
                        Expanded(child: Text("")),
                      ]
                    ),
                  ),
                  body:  ListView(children: <Widget>[
                    Text(""),
                      CheckboxListTile(
                        title: Text("Оповещать в " + profile["NotifyHour"].toString().padLeft(2,"0") + ":" + profile["NotifyMinute"].toString().padLeft(2,"0")),
                        value:  profile["Notify"] == true,
                         onChanged: (val) async {
                          var res = await showTimePicker(context: context, initialTime: TimeOfDay.now());
                          setState(() {
                            if (res == null) {
                              profile["Notify"] = false;
                              setupLocalNotifications(null);
                            } else {
                              profile["Notify"] = true;
                              profile["NotifyHour"] = res.hour;
                              profile["NotifyMinute"] = res.minute;
                              setupLocalNotifications(Time(res.hour, res.minute, 0));
                            }
                          });
                          saveProfile(profile);
                      }),
                    Text("Пол"),
                    RadioListTile(
                        groupValue: profile["Gender"],
                        title: Text("Мужской"),
                        value: "male",
                        onChanged: (val) {
                            setState(() {
                                profile["Gender"] = val;
                                saveProfile(profile);
                            });
                        },
                    ),
                    RadioListTile(
                        groupValue: profile["Gender"],
                        title: Text("Женский"),
                        value: "female",
                        onChanged: (val) {
                            setState(() {
                                profile["Gender"] = val;
                                saveProfile(profile);
                            });
                        },
                    ),
                    TextFormField(
                      decoration: InputDecoration(labelText: "Год рождения"),
                      keyboardType: TextInputType.number,
                      maxLines: null,
                      initialValue: profile["BirthYear"].toString(),
                      onChanged: (val ){
                          profile["BirthYear"] = int.parse(val);
                          saveProfile(profile);
                      },
                    ),
                    TextFormField(
                      decoration: InputDecoration(labelText: "Рост (см)"),
                      keyboardType: TextInputType.number,
                      maxLines: null,
                      initialValue: profile["Height"].toString(),
                      onChanged: (val ){
                          profile["Height"] = int.parse(val);
                          saveProfile(profile);
                      },
                    ),
                    TextFormField(
                      decoration: InputDecoration(labelText: "Вес (кг)"),
                      keyboardType: TextInputType.number,
                      maxLines: null,
                      initialValue: profile["Weight"].toString(),
                      onChanged: (val ){
                          profile["Weight"] = int.parse(val);
                          saveProfile(profile);
                      },
                    ),
                    ExpansionTile(
                      title: Text("При наличии заболевания"),
                      children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(labelText: "Год начала симптомов"),
                        keyboardType: TextInputType.number,
                        maxLines: null,
                        initialValue: profile["IllYear"].toString(),
                        onChanged: (val ){
                            profile["IllYear"] = int.parse(val);
                            saveProfile(profile);
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: "Год постановки диагноза"),
                        keyboardType: TextInputType.number,
                        maxLines: null,
                        initialValue: profile["DiagnosisYear"].toString(),
                        onChanged: (val ){
                            profile["DiagnosisYear"] = int.parse(val);
                            saveProfile(profile);
                        },
                      ),
                      TypeAheadField(
                        direction: AxisDirection.up,
                        textFieldConfiguration: TextFieldConfiguration(
                          controller: _diagnosisCtl,
                          decoration: InputDecoration(labelText: "Диагноз"),
                        ),
                        suggestionsCallback: (pattern) async {
                          return await suggest("diagnosis", pattern);
                        },
                        itemBuilder: (context, suggestion) {
                          return ListTile(
                            title: Text(suggestion),
                          );
                        },
                        onSuggestionSelected: (suggestion) {
                          _diagnosisCtl.text = suggestion;
                                profile["Diagnosis"] = suggestion;
                                saveProfile(profile);
                        },
                      ),

                      TypeAheadField(
                        direction: AxisDirection.up,
                        keepSuggestionsOnSuggestionSelected: true,
                        textFieldConfiguration: TextFieldConfiguration(
                          controller: _symptomsCtl,
                          decoration: InputDecoration(labelText: "Симптомы"),
                          maxLines: 5
                        ),
                        suggestionsCallback: (pattern) async {
                          return await suggest("symptoms", pattern);
                        },
                        itemBuilder: (context, suggestion) {
                          return ListTile(
                            title: Text(suggestion),
                          );
                        },
                        onSuggestionSelected: (suggestion) {
                            var cc =_symptomsCtl.text.split("\n");
                            cc.removeLast();
                            cc.add(suggestion);
                            _symptomsCtl.text = cc.join("\n") + "\n";
                            profile["Symptoms"] = _symptomsCtl.text;
                            saveProfile(profile);
                            _symptomsCtl.selection = TextSelection.fromPosition(TextPosition(offset: _symptomsCtl.text.length));
                        },
                      ),
                      TypeAheadField(
                        direction: AxisDirection.up,
                        keepSuggestionsOnSuggestionSelected: true,
                        textFieldConfiguration: TextFieldConfiguration(
                          controller: _medicationCtl,
                          decoration: InputDecoration(labelText: "Лечение"),
                          maxLines: 5
                        ),
                        suggestionsCallback: (pattern) async {
                          return await suggest("medication", pattern);
                        },
                        itemBuilder: (context, suggestion) {
                          return ListTile(
                            title: Text(suggestion),
                          );
                        },
                        onSuggestionSelected: (suggestion) {
                            var cc =_medicationCtl.text.split("\n");
                            cc.removeLast();
                            cc.add(suggestion);
                            _medicationCtl.text = cc.join("\n") + "\n";
                            profile["Medication"] = _medicationCtl.text;
                            saveProfile(profile);
                            _medicationCtl.selection = TextSelection.fromPosition(TextPosition(offset: _medicationCtl.text.length));
                        },
                      ),
                      TypeAheadField(
                        keepSuggestionsOnSuggestionSelected: true,
                        direction: AxisDirection.up,
                        textFieldConfiguration: TextFieldConfiguration(
                          controller: _diseasesCtl,
                          decoration: InputDecoration(labelText: "Другие заболевания "),
                          maxLines: 5
                        ),
                        suggestionsCallback: (pattern) async {
                          return await suggest("diseases", pattern);
                        },
                        itemBuilder: (context, suggestion) {
                          return ListTile(
                            title: Text(suggestion),
                          );
                        },
                        onSuggestionSelected: (suggestion) {
                            var cc =_diseasesCtl.text.split("\n");
                            cc.removeLast();
                            cc.add(suggestion);
                            _diseasesCtl.text = cc.join("\n") + "\n";
                            profile["Diseases"] = _diseasesCtl.text;
                            saveProfile(profile);
                            _diseasesCtl.selection = TextSelection.fromPosition(TextPosition(offset: _diseasesCtl.text.length));
                        },
                      ),
                      ],
                    ),
                  ]),
                );
    });
  }
}


var mainCtl = PageController(initialPage: 0);

class YouResearchApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<int>>(
          future: getRootList(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return MaterialApp(
                home: PageView(
                controller: mainCtl,
                children: <Widget>[
                  Image.asset("assets/splash.png")
                ], 
                ));
            }
            // if (snapshot.error != null) {
            //   return Text(snapshot.error);
            // }
            // if (snapshot.data.statusCode != 200) {
            //   return Text(snapshot.data.statusCode.toString() + " status code:" + snapshot.data.body);
            // }

            return FutureBuilder<Map<String,dynamic>>(
              future: getProfile(),
              builder: (context, profSnapshot) {
              if (!profSnapshot.hasData) {
                return MaterialApp(
                  home: PageView(
                  controller: mainCtl, //if profile is not filled - redirect to profile page
                  children: <Widget>[
                    Image.asset("assets/splash.png")
                  ], 
                  ));
              }

              mainCtl.jumpToPage(profSnapshot.data["BirthYear"] == 0 ? 1:0);
              return MaterialApp(
                home: PageView(
                  controller: mainCtl, //if profile is not filled - redirect to profile page
                children: <Widget>[
                  AnswerWidget(jsonDecode(utf8.decode(snapshot.data))),
                  ProfileWidget(),
                ], 
                ));
                  
                }
            );
          });
          
  }
}

void rebuildAllChildren(BuildContext context) {
  void rebuild(Element el) {
    el.markNeedsBuild();
    el.visitChildren(rebuild);
  }
  (context as Element).visitChildren(rebuild);
}

class Answers {
  static Map<String, double> current = Map<String,double>();
}

class ListWidget extends StatelessWidget {
  final List<dynamic> children;
  final String title;
  ListWidget(Map<String, dynamic> data) : 
    title=data["title"],
    children=data["list"];

  @override
  Widget build(BuildContext context) {
    var cc = List<Widget>();
    var first = true;
    for (Map<String,dynamic> ch in children) {
      Widget w;
      switch(ch["type"]) {
        case "list":
          w = ListWidget(ch);
          break;
        default:
          w = LikertWidget(ch);
          break;
      }
      cc.add(Padding(
        padding: EdgeInsets.fromLTRB(8,(first?20:0),0,0),
        child: w,
      ));
      first = false;
    }
    if (title == "") {
      return ListView(children: cc,primary: true,);
    }
    cc.add(new GestureDetector(
      onTap: () async {
        await launch("mailto:artem.and.co@gmail.com?subject=Хочу добавить пункт&body=(напиши какой пункт и куда нужно добавить)");
      },
      child: ListTile(title:Text("+ добавить", style: TextStyle(color: Colors.grey))))
      );
    return ExpansionTile(
      title: Text(title),
      initiallyExpanded: true,
      children: cc,
    );
  }
}


class LikertWidget extends StatefulWidget {
    final String title;
    final String id;

  LikertWidget(Map<String, dynamic> data) : 
    id=data["id"],
    title=data["title"];

    @override
    LikertWidgetState createState() => LikertWidgetState();
}

class LikertWidgetState extends State<LikertWidget> with AutomaticKeepAliveClientMixin {
  bool get wantKeepAlive => true;
    double _currentSliderValue = 0;
    Color _currentSliderColor = Colors.blue.withAlpha(100);
    @override
    Widget build(BuildContext context) {
        return Column(
              children: <Widget>[
                  Text(widget.title, textScaleFactor: 1.5),
                Padding(
                  padding: EdgeInsets.fromLTRB(16,0,16,0),
                  child:Slider(
                      value: _currentSliderValue,
                      min: 0,
                      max: 100,
                      activeColor: _currentSliderColor,
                      onChangeStart: (double value){
                        setState(() {
                          _currentSliderValue = value;
                          _currentSliderColor = Colors.blue;
                          if (value == 0) {
                            Answers.current.remove(widget.id);
                          } else {
                            Answers.current[widget.id] = value.round()/100;
                          }
                        });
                      },
                      onChanged: (double value) {
                        setState(() {
                          _currentSliderValue = value;
                          _currentSliderColor = Colors.blue;
                          if (value == 0) {
                            Answers.current.remove(widget.id);
                          } else {
                            Answers.current[widget.id] = value.round()/100;
                          }
                        });
                      },
                    ))]);
    }
}

void main() { 
  WidgetsFlutterBinding.ensureInitialized();
  runApp(YouResearchApp());
}

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

void setupLocalNotifications(Time time) async {
  if (time == null) {
    flutterLocalNotificationsPlugin.cancelAll();
  }
  //setting up notification
  var androidPlatformChannelSpecifics =  AndroidNotificationDetails('Daily', 'Daily', 'Notification for daily youresearch answer submission');
  var iOSPlatformChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.showDailyAtTime(
    0,
    "Время записывать свой день",
    'Мы хотим знать как ты себя чувствуешь :)',
    time,
    platformChannelSpecifics);

  //  handling notification on click
  // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  var initializationSettingsAndroid = AndroidInitializationSettings('@drawable/ic_launcher');
  var initializationSettingsIOS = IOSInitializationSettings(
      //onDidReceiveLocalNotification: onDidReceiveLocalNotification,
      );
  var initializationSettings = InitializationSettings(
      initializationSettingsAndroid, initializationSettingsIOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
      if (payload != null) {
        debugPrint('notification payload: ' + payload);
      }
      
  });
}